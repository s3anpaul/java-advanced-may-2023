import java.util.Arrays;
import java.util.Scanner;

public class RecursiveArraySum {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int[] arr = Arrays.stream(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        sum(arr, 0, 0);

        System.out.println( sumArr(arr, 0));

    }
    // from lecture
    private static int sumArr(int[] arr, int index) {
        if (index>=arr.length){
            return 0;
        }

        return arr[index] + sumArr(arr,index + 1);
    }
    // My implementation
    private static void sum(int[] arr, int index, int sum) {
        if (index>=arr.length){
            System.out.println(sum);
            return;}
        sum += arr[index++];
        sum(arr,index, sum);

    }

    private static void print(int i) {
        if (i > 10) {
            return;
        }
        System.out.print(i);
        print(++i);
        System.out.println("after method " + i );
    }
}
