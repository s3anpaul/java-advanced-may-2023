import java.util.Arrays;
import java.util.Scanner;

public class TheMatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] rowsAndCols = Arrays.stream(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        int rows = rowsAndCols[0];
        int cols = rowsAndCols[1];

        char[][] matrix = new char[rows][cols];


        for (int i = 0; i < rows; i++) {
            matrix[i] = scanner.nextLine().replaceAll("\\s+", "").toCharArray();
        }

        char newColor = scanner.nextLine().charAt(0);

        int[] clickedPixels = Arrays.stream(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

        int clickedRow = clickedPixels[0];
        int clickedCol = clickedPixels[1];

        char oldColor = matrix[clickedRow][clickedCol];


        paint(matrix, newColor, oldColor, clickedRow, clickedCol);

        for (char[] chars : matrix) {
            for (char aChar : chars) {
                System.out.print(aChar);
            }
            System.out.println();
        }

    }

    private static void paint(char[][] matrix, char newColor, char oldColor, int clickedRow, int clickedCol) {

        if (isOutOfBounds(matrix,clickedRow,clickedCol))
            return;
        else if (matrix[clickedRow][clickedCol] != oldColor)
            return;

        matrix[clickedRow][clickedCol] = newColor;

        paint(matrix, newColor, oldColor, clickedRow + 1, clickedCol);
        paint(matrix, newColor, oldColor, clickedRow - 1, clickedCol);
        paint(matrix, newColor, oldColor, clickedRow, clickedCol + 1);
        paint(matrix, newColor, oldColor, clickedRow, clickedCol - 1);

    }

    private static boolean isOutOfBounds(char[][] matrix, int r, int c) {

        return r < 0 || r >= matrix.length || c < 0 || c >= matrix[r].length;
    }
}



