import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class AppliedArithmetic {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> list = Arrays.stream(scanner.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        String command = scanner.nextLine();

        UnaryOperator<List<Integer>> addFunction = a -> a.stream().map(b -> b +1).collect(Collectors.toList());
        UnaryOperator<List<Integer>> subtractFunction = a -> a.stream().map(b -> b -1).collect(Collectors.toList());
        UnaryOperator<List<Integer>> multiplyFunction = a -> a.stream().map(b -> b *2).collect(Collectors.toList());
        Consumer<List<Integer>> print = a -> System.out.println(a.stream().map(String::valueOf).collect(Collectors.joining(" ")));

        while (!command.equals("end")){
            switch (command) {
                case "add":
                    list = addFunction.apply(list);
                    break;
                case "multiply":

                    list = multiplyFunction.apply(list);

                    break;
                case "subtract":
                    list = subtractFunction.apply(list);
                    break;

                case "print":
                    print.accept(list);
                    break;

            }
            command = scanner.nextLine();
        }
    }
}
