import java.util.*;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class CustomComparator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> command = Arrays.stream(scanner.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());
        Comparator<Integer> myCompare = (first,second) ->{
          if (first%2 == 0 && second % 2 != 0){
              return  -1;

          } else if (first%2 != 0 && second % 2 == 0){
              return 1;
          }
          return first.compareTo(second);
        };

        Collections.sort(command,myCompare);
        System.out.println(command.stream().map(String::valueOf).collect(Collectors.joining(" ")));


    }
}
