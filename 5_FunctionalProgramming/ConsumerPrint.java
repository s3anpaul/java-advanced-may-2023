import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ConsumerPrint {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> commands = Arrays.stream(scanner.nextLine().split("\\s+")).collect(Collectors.toList());

        Consumer<List<String>> consumer = a -> a.stream().forEach(b -> System.out.println("Sir " + b));
        consumer.accept(commands);
    }
}
