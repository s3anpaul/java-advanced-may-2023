import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListOfPredicates {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int range = Integer.parseInt(scanner.nextLine());

        List<Integer> divisibleNumbers = Arrays.stream(scanner.nextLine().split("\\s+")).map(Integer::parseInt).collect(Collectors.toList());

        IntPredicate myPredicate = a -> {
            Boolean allDivisible = false;
            for (int i = 0; i < divisibleNumbers.size(); i++) {
                   allDivisible = a % divisibleNumbers.get(i) == 0;
            }
            return allDivisible;
        };

        List myNewList =IntStream.range(1, range + 1).filter(myPredicate).mapToObj(String::valueOf).collect(Collectors.toList());

        System.out.println(myNewList.stream().collect(Collectors.joining(" ")));
    }
}
