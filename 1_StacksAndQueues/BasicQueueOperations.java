import java.util.ArrayDeque;
import java.util.Scanner;

public class BasicQueueOperations {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numberOfElement = scanner.nextInt();
        int removeElements = scanner.nextInt();
        int searchNumber = scanner.nextInt();
        int lastNumber;
        ArrayDeque<Integer> elements = new ArrayDeque<Integer>();

        for (int i = 0; i < numberOfElement; i++) {
            elements.offer(scanner.nextInt());
        }
        for (int i = 0; i < removeElements; i++) {
            elements.poll();
        }

        if (elements.isEmpty()) {
            System.out.println(0);
        } else {
            lastNumber = elements.peek();
            while (!elements.isEmpty()) {
                if (elements.peek() == searchNumber) {
                    System.out.println("true");
                    break;
                } else {
                    int currentNumber = elements.poll();
                    if (lastNumber >= currentNumber) {
                        lastNumber = currentNumber;
                    }

                }
            }
            if (elements.isEmpty()) {
                System.out.println(lastNumber);
            }
        }
    }
}
