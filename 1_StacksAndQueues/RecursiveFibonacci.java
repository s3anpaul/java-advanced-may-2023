import java.util.Arrays;
import java.util.Scanner;

public class RecursiveFibonacci {
    public static  long[] myLong;
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int number = Integer.parseInt(sc.nextLine());
         myLong = new long[number + 1];

        System.out.println(fibonacci(number));

    }
    public static long fibonacci(int num){
        if (num < 2){
            return 1;
        }
        if (myLong[num] != 0){
            return myLong[num];
        }
        myLong[num] = fibonacci(num - 1 ) + fibonacci( num  - 2);
        return myLong[num];
    }
}
