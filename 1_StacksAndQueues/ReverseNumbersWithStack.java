import java.util.ArrayDeque;
import java.util.Scanner;

public class ReverseNumbersWithStack {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] elements = scanner.nextLine().split(" ");
        ArrayDeque<String> stack = new ArrayDeque<String>();
        for (int i = 0; i < elements.length; i++) {
            stack.push(elements[i]);
        }
        for (int i = 0; i < elements.length; i++) {
            System.out.print(stack.pop() + " ");
        }
    }
}
