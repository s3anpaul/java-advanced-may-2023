import java.util.ArrayDeque;
import java.util.Scanner;

public class BalancedParentheses {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        ArrayDeque<Character> openStack = new ArrayDeque<Character>();
        ArrayDeque<Character> closeQueue = new ArrayDeque<Character>();
        boolean balances = false;

        if (command.length()%2 != 0){
            System.out.println("NO");
        } else{
            for (int i = 0; i < command.length()/2; i++) {
                openStack.push(command.charAt(i));
            }
            for (int i = command.length()/2; i < command.length(); i++) {
                closeQueue.offer(command.charAt(i));
            }
            for (int i = 0; i < command.length()/2; i++) {
                char currentOpen = openStack.pop();
                char currentClose = closeQueue.poll();
                if (currentOpen == '(' && currentClose == ')' || currentOpen == '{' && currentClose == '}'
                        || currentOpen == '[' && currentClose == ']') {
                    balances = true;
                } else{
                    balances = false;
                    break;
                }
            }
            if (balances)
                System.out.println("YES");
            else
                System.out.println("NO");
        }


    }
}
