import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class OsPlanning {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        ArrayDeque<Integer> stackTasks = new ArrayDeque<>();

        ArrayDeque<Integer> queueThreads = new ArrayDeque<>();

        List<Integer> numbers = Arrays.stream(scanner.nextLine().split(", ")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < numbers.size(); i++) {
            stackTasks.push(numbers.get(i));
        }
        numbers = Arrays.stream(scanner.nextLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());
        for (int i = 0; i < numbers.size(); i++) {
            queueThreads.add(numbers.get(i));
        }

        int killedTask = Integer.parseInt(scanner.nextLine());

        int currentTask = stackTasks.peek();
        int currentThread = queueThreads.peek();

        while (currentTask != killedTask){


            if (currentThread >= currentTask){
                stackTasks.pop();
                queueThreads.remove();
            }else{
                queueThreads.remove();
            }
             currentTask = stackTasks.peek();
             currentThread = queueThreads.peek();

        }
        System.out.printf("Thread with value %d killed task %d",currentThread, currentTask);
        System.out.println();
        for (Integer number: queueThreads
             ) {
            System.out.print(number + " ");
        }
    }


}

