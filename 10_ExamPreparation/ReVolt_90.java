import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static int startRow;
    public static int startColumn;
    public static boolean hasWon;
    public static String command;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int matrixSize = Integer.parseInt(scanner.nextLine());
        byte[][] matrix = new byte[matrixSize][matrixSize];
        int commands = Integer.parseInt(scanner.nextLine());
        hasWon = false;


        for (int i = 0; i < matrixSize; i++) {
            matrix[i] = scanner.nextLine().getBytes();
            for (int j = 0; j < matrixSize; j++) {
                if (matrix[i][j] == 'f') {
                    startRow = i;
                    startColumn = j;
                }
            }
        }


        int i = 0;
        while (!hasWon && i < commands) {
            command = scanner.nextLine();

            switch (command) {
                case "up":
                    int newRow = startRow - 1;
                    movePlayer(matrix, newRow, startColumn);
                    break;
                case "down":
                    int newRow1 = startRow + 1;
                    movePlayer(matrix, newRow1, startColumn);
                    break;
                case "left":
                    int newColumn = startColumn - 1;
                    movePlayer(matrix, startRow, newColumn);
                    break;
                case "right":
                    int newColumn1 = startColumn + 1;
                    movePlayer(matrix, startRow, newColumn1);
                    break;
            }
            i += 1;
        }


        if (hasWon) {
            System.out.println("Player won!");
        } else {
            System.out.println("Player lost!");
        }
        printMatrix(matrix);

    }


    ////

    private static void movePlayer(byte[][] matrix, int r, int c) {
        if (isOutOfBounds(matrix, r, c)) {
            if (r < 0)
                r = matrix.length - 1;
            else if (c < 0)
                c = matrix.length - 1;
            else if (r == matrix.length)
                r = 0;
            else if (c == matrix.length)
                c = 0;
        }


        if (matrix[r][c] == 'T') {
            return;
        }
        if (matrix[r][c] == 'F') {
            hasWon = true;
        }
        if (matrix[r][c] == 'B') {
            if ("down".equals(command)) {
                r += 1;
//                c += 1;
            } else if ("up".equals(command)) {
                r -= 1;
            } else if ("left".equals(command)) {
                c -= 1;
            } else if ("right".equals(command)) {
                c += 1;
            }
        }

        if (isOutOfBounds(matrix, r, c)) {
            if (r < 0)
                r = matrix.length - 1;
            else if (c < 0)
                c = matrix.length - 1;
            else if (r == matrix.length)
                r = 0;
            else if (c == matrix.length)
                c = 0;
        }

        matrix[startRow][startColumn] = '-';
        matrix[r][c] = 'f';

        startRow = r;
        startColumn = c;


    }

    private static boolean isOutOfBounds(byte[][] matrix, int r, int c) {

        return r < 0 || r >= matrix.length || c < 0 || c >= matrix.length;
    }


    private static void printMatrix(byte[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {

            for (int j = 0; j < matrix.length; j++) {
                System.out.print((char) matrix[i][j]);
            }
            System.out.println();
        }
    }
}

