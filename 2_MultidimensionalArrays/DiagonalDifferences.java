import java.util.Arrays;
import java.util.Scanner;

public class DiagonalDifferences {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sizeMatrix = Integer.parseInt(scanner.nextLine());
        int[][] matrix = new int[sizeMatrix][sizeMatrix];
        int primaryDiagonal = 0;
        int secondaryDiagonal = 0;

        for (int i = 0; i < sizeMatrix; i++) {
            matrix[i] = Arrays.stream(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        }

        for (int i = 0; i < sizeMatrix; i++) {
            primaryDiagonal = primaryDiagonal + matrix[i][i];
        }
        for (int i = 0; i < sizeMatrix; i++) {
            secondaryDiagonal = secondaryDiagonal + matrix[i][sizeMatrix - i -1];
        }
        System.out.println(java.lang.Math.abs(primaryDiagonal - secondaryDiagonal));
    }
}
