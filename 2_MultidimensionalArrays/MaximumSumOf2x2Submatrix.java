import java.util.Arrays;
import java.util.Scanner;

public class MaximumSumOf2x2Submatrix {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int sizeMatrix[] = Arrays.stream(scanner.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
        int matrix[][] = new int[sizeMatrix[0]][sizeMatrix[1]];
        int Sum = Integer.MIN_VALUE;
        int maxMatrix[][] = new int[2][2];

        for (int i = 0; i < matrix.length; i++) {
            matrix[i] = Arrays.stream(scanner.nextLine().split(", ")).mapToInt(Integer::parseInt).toArray();
        }

//        for (int[] arr : matrix) {
//            System.out.println(Arrays.toString(arr));
//        }
        for (int i = 0; i < sizeMatrix[0]-1; i++) {
            for (int j = 0; j < sizeMatrix[1] -1; j++) {
                int currentSum = 0;
                currentSum += matrix[i][j] + matrix[i+1][j] + matrix[i][j+1] + matrix[i+1][j+1];

                if (currentSum> Sum){
                    Sum = currentSum;
                    maxMatrix[0][0] = matrix[i][j];
                    maxMatrix[0][1] = matrix[i][j+1];
                    maxMatrix[1][0] = matrix[i+1][j];
                    maxMatrix[1][1] = matrix[i+1][j+1];
                }

            }
        }
        for (int[] arr: maxMatrix) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i]+ " ");
            }
            System.out.println();
        }
        System.out.println(Sum);
    }
}
