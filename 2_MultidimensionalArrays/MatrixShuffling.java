import java.util.Arrays;
import java.util.Scanner;

public class MatrixShuffling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] tokens = Arrays.stream(scanner.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        int rows = tokens[0];
        int cols = tokens[1];
        String[][] matrix = new String[rows][cols];
        fill(matrix, scanner);
        String[] input = scanner.nextLine().split("\\s+");
        while (!input[0].equals("END")) {
            if (!input[0].equals("swap")  || input.length != 5 ) {
                System.out.println("Invalid input!");
            } else {
                int firstRow = Integer.parseInt(input[1]);
                int firstCol = Integer.parseInt(input[2]);
                int secondRow = Integer.parseInt(input[3]);
                int secondCol = Integer.parseInt(input[4]);
                if (rows <= firstRow || rows <= secondRow || cols <= firstCol || cols <= secondCol) {
                    System.out.println("Invalid input!");
                } else {
                    String currentToken = matrix[firstRow][firstCol];
                    matrix[firstRow][firstCol] = matrix[secondRow][secondCol];
                    matrix[secondRow][secondCol] = currentToken;
                    printMatrix(matrix);
                }

            }
            input = scanner.nextLine().split("\\s+");
        }

    }

    private static void printMatrix(String[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }

    private static void fill(String[][] matrix, Scanner scanner) {
        for (int row = 0; row < matrix.length; row++) {
            String[] arr = scanner.nextLine().split("\\s+");

            matrix[row] = arr;
        }
    }
}
