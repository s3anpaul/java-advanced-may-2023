import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class StringMatrixRotation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String stringDegrees = scanner.nextLine();
        String commands = scanner.nextLine();
        int counter = 0;
        int maxLength = 0;
        List<String> allCommands = new ArrayList<>();

        int degrees = Integer.parseInt(stringDegrees.substring(stringDegrees.indexOf('(') + 1, stringDegrees.indexOf(')')));

        while (!commands.equals("END")) {
            allCommands.add(commands);
            counter += 1;
            commands = scanner.nextLine();
        }
        for (String el : allCommands
        ) {
            if (maxLength <= el.length())
                maxLength = el.length();
        }
        char[][] matrix = new char[counter][maxLength];


        for (int i = 0; i < counter; i++) {
            for (int j = 0; j < allCommands.get(i).length(); j++) {
                matrix[i][j] = allCommands.get(i).charAt(j);
            }
        }
        if (degrees >= 360)
            degrees = degrees % 360;

        switch (degrees) {
            case 0:
                print360(matrix);
                break;
            case 90:
                print90(matrix);
                break;
            case 180:
                print180(matrix);
                break;
            case 270:
                print270(matrix);
                break;
        }


    }

    public static void print360(char[][] matrix) {
        for (char[] arr : matrix) {
            for (char el : arr) {
                System.out.print(el);
            }
            System.out.println();
        }
    }

    public static void print90(char[][] matrix) {
        int numbers = matrix[0].length;
        for (int i = 0; i < numbers; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[matrix.length - j - 1][i] < 32 || matrix[matrix.length - j - 1][i] > 127)
                    System.out.print(" ");
                else
                    System.out.print(matrix[matrix.length - j - 1][i]);
            }
            System.out.println();
        }
    }

    public static void print180(char[][] matrix) {
        int numbers = matrix[0].length;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < numbers; j++) {
                if (matrix[matrix.length - i - 1][numbers - 1 - j] < 32 || matrix[matrix.length - i - 1][numbers - 1 - j] > 127)
                    System.out.print(" ");
                else
                    System.out.print(matrix[matrix.length - i - 1][numbers - 1 - j]);
            }
            System.out.println();
        }
    }

    public static void print270(char[][] matrix) {
        int numbers = matrix[0].length;
        for (int i = 0; i < numbers; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[j][numbers - 1 - i] < 32 || matrix[j][numbers - 1 - i] > 127)
                    System.out.print(" ");
                else
                    System.out.print(matrix[j][numbers - 1 - i]);
            }
            System.out.println();
        }
    }
}
