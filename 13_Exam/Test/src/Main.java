import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayDeque<Integer> monsterQueue = new ArrayDeque<>();
        ArrayDeque<Integer> soldersStack = new ArrayDeque<>();

        String input = scanner.nextLine();

        Arrays.stream(input.split(",")).map(Integer::parseInt).forEach(monsterQueue::offer);
        input = scanner.nextLine();
        Arrays.stream(input.split(",")).map(Integer::parseInt).forEach(soldersStack::push);

        int numberMonsters = 0;


        while (!monsterQueue.isEmpty() && !soldersStack.isEmpty()){
            int currentMonster = monsterQueue.remove();
            int currentSolder = soldersStack.pop();

            if (currentMonster <= currentSolder){
                numberMonsters += 1;
                currentSolder -= currentMonster;
                if (currentSolder != 0 ){
                    int nextSolder;
                    if (soldersStack.isEmpty()){
                        nextSolder = currentSolder;
                    } else{
                        nextSolder = soldersStack.pop() + currentSolder;
                    }
                    soldersStack.push(nextSolder);

                }
                }  else{
                currentMonster -= currentSolder;
                monsterQueue.offer(currentMonster);
            }


        }

        if (monsterQueue.isEmpty() && soldersStack.isEmpty()){
            System.out.println("The soldier has been defeated.");
        } else if(monsterQueue.isEmpty()){
            System.out.println("All monsters have been killed!");
        } else{
            System.out.println("The soldier has been defeated.");
        }

        System.out.printf("Total monsters killed: %d",numberMonsters);
    }
}