package vendingSystem;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Drink {

    private String name;

    private BigDecimal price;

    private int volume;

    public Drink(String name, BigDecimal price, int volume) {
        this.name = name;
        this.price = price;
        this.volume = volume;
    }

    public String getName() {
        return name;
    }

    public int getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        String pattern = "##.0#";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String format = decimalFormat.format(price);
        return String.format("Name: %s, Price: $%s, Volume: %d ml",name,format,volume);
    }
}
