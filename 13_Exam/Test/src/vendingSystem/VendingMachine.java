package vendingSystem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class VendingMachine {
    private List<Drink> drinks;

    private int buttonCapacity;

    public VendingMachine(int buttonCapacity) {
        this.buttonCapacity = buttonCapacity;
        this.drinks = new ArrayList<>();
    }


    public int getCount() {
        return drinks.size();
    }

    public void addDrink(Drink drink) {

        if (buttonCapacity > getCount()){
            drinks.add(drink);
        }

    }

    public boolean removeDrink(String drinkName) {

        Drink drink = drinks.stream().filter(d -> d.getName().equals(drinkName)).findAny().orElse(null);

        if (drink==null){
            return false;
        } else{
            drinks.remove(drink);
            return true;
        }

    }

    public Drink getLongest() {

        Drink drink = drinks.stream().max(Comparator.comparingInt(Drink::getVolume)).stream().findFirst().orElse(null);
        return drink;
    }

    public Drink  getCheapest() {

        Drink drink = drinks.stream().min(Comparator.comparingInt(Drink::getVolume)).stream().findFirst().orElse(null);
        return drink;
    }

    public String buyDrink(String drinkName) {
        Drink drink = drinks.stream().filter(d -> d.getName().equals(drinkName)).findAny().orElse(null);

        return drink.toString();
    }

    public String report() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Drink drink : drinks) {
            stringBuilder.append(drink.toString());
            stringBuilder.append(System.lineSeparator());
        }
        return "Drinks available:" + System.lineSeparator() + stringBuilder;
    }
}
