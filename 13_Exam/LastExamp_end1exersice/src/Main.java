import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] size = scanner.nextLine().split("\\s+");
        int rows = Integer.parseInt(size[0]);
        int cols = Integer.parseInt(size[1]);
        char[][] area = new char[rows][cols];
        boolean deliveryisLate = false;
        int manRow = -1;
        int manColumn = -1;
        char deliveryBoy = 'B';
        for (int i = 0; i < rows ; i++) {
            char[] row = scanner.nextLine().toCharArray();
            area[i] = row;
        }

        int firstRow = -1;
        int firstCol = -1;

        int rRow = -1;
        int rCol = -1;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (area[i][j] == 'B'){
                    manRow = i;
                    firstRow = i;
                    manColumn = j;
                    firstCol = j;
                }
            }
        }

        String command = scanner.nextLine();

        while (!"End".equals(command)){

            area[manRow][manColumn] = '.';

            if (command.equals("right") && manColumn != cols - 1){
                manColumn++;
            } else if (command.equals("left")&& manColumn != 0){
                manColumn--;
            } else if (command.equals("down")&& manRow != rows -1){
                manRow++;
            } else if (command.equals("up")&& manRow != 0){
                manRow--;
            } else{
                area[manRow][manColumn] = '.';
                deliveryisLate = true;
                System.out.println("The delivery is late. Order is canceled.");
                break;
            }

            if (area[manRow][manColumn] == '*'){
                if (command.equals("right") ){
                    manColumn--;
                } else if (command.equals("left")){
                    manColumn++;
                } else if (command.equals("down")){
                    manRow--;
                } else if (command.equals("up")){
                    manRow++;

            }
                command = scanner.nextLine();
                continue;
            } else if(area[manRow][manColumn] == 'P'){

                rRow = manRow;
                rCol = manColumn;
                deliveryBoy = 'R';
                System.out.println("Pizza is collected. 10 minutes for delivery.");
            } else if(area[manRow][manColumn] == 'A' && deliveryBoy == 'R'){
                area[manRow][manColumn] = 'P';
                System.out.println("Pizza is delivered on time! Next order...");
                break;
            }

            area[manRow][manColumn] = deliveryBoy;

//            for (int i = 0; i < rows; i++) {
//                for (int j = 0; j < cols; j++) {
//                    System.out.print(area[i][j] + " ");
//                }
//                System.out.println();
//            }

            command = scanner.nextLine();

        }
        if (!deliveryisLate){
        area[firstRow][firstCol] = 'B';}
        else{
            area[firstRow][firstCol] = ' ';
        }
        area[rRow][rCol] = 'R';
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.print(area[i][j]);
            }
            System.out.println();
        }


    }
}