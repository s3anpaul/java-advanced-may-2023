import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;

public class PeriodicTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TreeSet<String> commands = new TreeSet<>();
        int numberRows = Integer.parseInt(scanner.nextLine());
        String[] commandString = new String[numberRows];

        for (int i = 0; i < numberRows; i++) {
            commandString =scanner.nextLine().split("\\s+");
            for (int j = 0; j < commandString.length; j++) {
                commands.add(commandString[j]);
            }
        }
        for (String el: commands
             ) {
            System.out.print(el + " ");
        }

//        commands.stream().forEach(s -> System.out.print(s + " "));
//        String result = String.join(", ",commands);
//        System.out.println(result);
    }
}
