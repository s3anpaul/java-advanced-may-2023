import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class CountSymbols {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        byte[] text = scanner.nextLine().getBytes();
        TreeMap<Byte,Integer> map = new TreeMap<>();

        for (int i = 0; i < text.length; i++) {
            if (map.containsKey(text[i])){
                int current = map.get(text[i]) + 1;
                map.put(text[i],current);
            } else{
                map.put(text[i],1);
            }


        }

//        for (byte el: text
//             ) {
//            System.out.println((char) el);
//        }
        for (Map.Entry<Byte,Integer> entry : map.entrySet()
             ) {
            byte current = entry.getKey();
            System.out.println((char) current+ ": " + entry.getValue() + " time/s");
        }

    }
}
